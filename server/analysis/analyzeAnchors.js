'use strict'

const _ = require('lodash');
const url = require('url');

var analyzeAnchors = function ($, requestedUrl, result) {
  var r = _.cloneDeep(result)
  r.anchors = {
    totalAnchors: $('a').length,
    hostAnalysis: {
      internalAnchors: 0,
      incompleteAnchors: 0,
      externalAnchors: 0,
    },
    accessibilityAnalysis: {
      accessibleAnchors: 0,
      innaccessibleAnchors: 0
    }
  }
  $('a').each(function(i, anchor) {
    if (typeof anchor.attribs.rel !== 'undefined' && anchor.attribs.rel !== '') {
      r.anchors.accessibilityAnalysis.accessibleAnchors++
    }
    else {
      r.anchors.accessibilityAnalysis.innaccessibleAnchors++
    }

    try {
      var anchorUrl = url.parse(anchor.attribs.href)
    } catch(err) {
      // DGB 2016-08-15 17:57 
      // It happens when an anchor doesnt have an HREF attribute for example
      // when the anchor is there only to activate a javascript function or
      // a dummy placeholder
      r.anchors.hostAnalysis.incompleteAnchors++;
      return true; // DGB 2016-08-15 17:56 Like a continue
    }

    if ( anchorUrl.host ===  url.parse(requestedUrl).host) { 
      r.anchors.hostAnalysis.internalAnchors++;
    } else {
      r.anchors.hostAnalysis.externalAnchors++;
    }
  });
  return r;
}


module.exports = analyzeAnchors
