'use strict'

const _ = require('lodash');

var analyzeLoginForm = function($, result) {
  var r = _.cloneDeep(result)
  r.hasLoginForm = false;
  if ($("input[type=password]").length) {
    r.hasLoginForm = true;
  }
  return r;
}


module.exports = analyzeLoginForm
