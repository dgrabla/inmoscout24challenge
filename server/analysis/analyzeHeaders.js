'use strict'

const _ = require('lodash');

var analyzeHeaders = function($, result) {
  let r = _.cloneDeep(result)
  let headers = ["h1", "h2", "h3", "h4", "h5", "h6"];
  r.headerCount = {};
  for (let i=0; i < headers.length; i++) {
    r.headerCount[headers[i]] = $(headers[i]).length;
  }
  return r;
}

module.exports = analyzeHeaders
