'use strict'

const _ = require('lodash');
const doctypes = require('doctypes')

var analyzeHtmlVersion = function(body, result) {
  var r = _.cloneDeep(result)
  r.htmlVersion = 'unknown spec';
  for (var doctype in doctypes) {
    if (!doctypes.hasOwnProperty(doctype)) {
        continue;
    }
    if (body.toLowerCase().indexOf(doctypes[doctype].toLowerCase()) !== -1) {
      r.htmlVersion = doctype;
    }
  }
  return r;
}
module.exports = analyzeHtmlVersion
