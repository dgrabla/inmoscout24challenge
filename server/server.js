
'use strict';

const express = require('express');
const bodyParser = require('body-parser')

var urlAnalyzer = require('./urlAnalyzer')
var app = express();

const DEBUG = (process.env.NODE_ENV === 'development') ? true : false;
if (!DEBUG) {
  console.log = function() {}
}

// DGB 2016-08-15 16:57 Support for URL encoded POST data
app.use(bodyParser.urlencoded({
  extended: true
}));

// DGB 2016-08-16 14:32 Normally the static content would be server by NGINX or
// cached by a delivery network like cloudfront etc
app.get('*', express.static('frontend-build'))

app.get(['/apiv1/urlanalizer'], function (req, res) {
  res.send("URL analyzer microservice.\nTest me with a POST request with a URL encoded 'url' parameter at /apiv1/urlAnalyzer.");
})

app.post('/apiv1/urlAnalyzer', urlAnalyzer);

app.listen(8080, function() {
  console.info('\n✓ Service started on port 8080.\n')
});
