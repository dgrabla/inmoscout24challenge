'use strict';

const should = require('should');
var pageTestBody = require('./pageTestBody');

var analyzeHtmlVersion = require('../analysis/analyzeHtmlVersion')


describe('analyzeHtmlVersion',function(){
  var $;
  var result = {};

  result = analyzeHtmlVersion(pageTestBody, {})

  it('should have HTML htmlVersion',function(){
    result.should.have.property('htmlVersion','html');
  });

});
