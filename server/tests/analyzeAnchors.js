'use strict';

const should = require('should');
const cheerio = require('cheerio');
var pageTestBody = require('./pageTestBody');

var analyzeAnchors = require('../analysis/analyzeAnchors')


describe('analyzeAnchors',function(){
  var $;
  var host='localhost';
  var result = {};

  result = analyzeAnchors(cheerio.load(pageTestBody), host, {})

  it('should have anchors',function(){
    result.should.have.property('anchors');
  });

  it('should have 2 totalAnchors',function(){
    result.anchors.should.have.property('totalAnchors',2);
  });

  it('should have one internal anchor',function(){
    result.anchors.hostAnalysis.should.have.property('internalAnchors',1);
  });

  it('should have one external anchor',function(){
    result.anchors.hostAnalysis.should.have.property('externalAnchors',1);
  });
 
 
  it('should have accessibility Analysis',function(){
    result.anchors.should.have.property('accessibilityAnalysis');

  });
  
  it('should have one accesible anchor',function(){
    result.anchors.accessibilityAnalysis.should.have.property('accessibleAnchors',1);
  });

  it('should have one innaccesible anchor',function(){
    result.anchors.accessibilityAnalysis.should.have.property('innaccessibleAnchors',1);
  });


});
