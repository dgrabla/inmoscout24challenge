'use strict';

const should = require('should');
const cheerio = require('cheerio');
var pageTestBody = require('./pageTestBody');

var analyzeHeaders = require('../analysis/analyzeHeaders')


describe('analyzeHeaders',function(){
  var $;
  var result = {};

  result = analyzeHeaders(cheerio.load(pageTestBody), {})

  it('should have headerCount',function(){
    result.should.have.property('headerCount');
  });

  it('should have one H1',function(){
    result.headerCount.should.have.property('h1',1);
  });

  it('should have two H2',function(){
    result.headerCount.should.have.property('h2',2);
  });

  it('should have zero H3',function(){
    result.headerCount.should.have.property('h3',0);
  });

});
