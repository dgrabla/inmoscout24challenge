'use strict';

const should = require('should');
const cheerio = require('cheerio');
var pageTestBody = require('./pageTestBody');

var analyzeLoginForm = require('../analysis/analyzeLoginForm')


describe('analyzeLoginForm',function(){
  var $;
  var result = {};

  result = analyzeLoginForm(cheerio.load(pageTestBody), {})

  it('should have login Form',function(){
    result.should.have.property('hasLoginForm',true);
  });

});
