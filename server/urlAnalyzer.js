'use strict'

const _ = require('lodash');
const url = require('url');
const validUrl = require('valid-url');
const request = require('request');
const cheerio = require('cheerio');

var analyzeHeaders = require('./analysis/analyzeHeaders')
var analyzeHtmlVersion = require('./analysis/analyzeHtmlVersion')
var analyzeLoginForm = require('./analysis/analyzeLoginForm')
var analyzeAnchors = require('./analysis/analyzeAnchors')

module.exports = function (req, res) {
  console.log('\nGot a request: ' + req.body.url);
  var clientAnswer = {};
  if (validUrl.isWebUri(req.body.url)) {
    var requestedUrl = url.format(req.body.url); // Throws TypeError if URL doesnt have expected format
  } else {
    // url.format will throw TypeError, if it is not an url
    clientAnswer = {status: 400, message: 'URL is not a proper formatted URL like protocol://user:pass@host.com:8080/p/a/t/h?query=string#hash'}
    console.log('clientAnswer: ', clientAnswer)
    res.status(400)
    res.json(clientAnswer)
    return
  }

  console.log('URL seems to be valid, starting HTTP/S request');
  request(requestedUrl, function (error, response, body) {
    if (!error && response.statusCode === 200) {
      console.log('Retrieved URL OK');
      let $ = cheerio.load(body);
      clientAnswer.url = requestedUrl;
      clientAnswer.pageTitle = $('title').text();
      clientAnswer = analyzeHtmlVersion(body, clientAnswer);
      clientAnswer = analyzeHeaders($, clientAnswer);
      clientAnswer = analyzeAnchors($, requestedUrl, clientAnswer);
      clientAnswer = analyzeLoginForm($, clientAnswer)
    } else if (error) {
      console.log('Got error:', error);
      clientAnswer.message = 'Error retrieving URL';
      clientAnswer.error = error.message;
      clientAnswer.status = 400;
      res.status(400);
    } else if (response.statusCode !== 200) {
      console.log('Got statusCode:', response.statusCode);
      clientAnswer.message = 'Unexpected status code (requires auth?)';
      clientAnswer.status = 400;
      clientAnswer.urlStatusCode = response.statusCode;
      res.status(400);
    }  
    console.log('clientAnswer: ', clientAnswer)
    res.json(clientAnswer);
  });
};
