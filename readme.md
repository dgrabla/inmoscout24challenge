# A simple Web-Application for Analyzing Web-Sites

Here is a web-application that does some analysis of a web-page/URL.
The application shows a form with an input field where the user can type in the URL of the
webpage being analyzed and a button for sending the input to the server as well as to trigger the server-side analysis process.

After processing the results are shown to the user in terms of a simple table below to the
form. 

## How to install and use

- Clone repository
- Run `> npm install`
- Run `> (NODE_ENV=development node server.js)` to run it on debug mode (it dumps
  log messages)

# Architecture

Backend - Express, Cheerio, Mocha (tests), Eslint (linter)

Frontend - React, Babel, Webpack, Bootstrap, Eslint (linter)

There are not a lot of comments, but the code is simple enough that it is explained by itself. It is also modular, easy to maintain and easy to extend. Functions are inmutable, tests assure future consistency.


# Backend

Server side the app uses Express as web application framework to expose a single functional API endpoint, some core node.js libraries (url, request), a few tools (url-valid, lodash), and cheerio for DOM processing.

## Assumptions

### HTML Version

All sites declare the version on the HTML (*!DOCTYPE*) tag. I used the
nomenclature used by the quasi-standard `doctypes` NPM module. These are the possible HTML versions (keys) and the expected HTML tags as of 08.2016:

```
{
  'html': '<!DOCTYPE html>',
  'xml': '<?xml version="1.0" encoding="utf-8" ?>',
  'transitional': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
  'strict': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
  'frameset': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">',
  '1.1': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">',
  'basic': '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML Basic 1.1//EN" "http://www.w3.org/TR/xhtml-basic/xhtml-basic11.dtd">',
  'mobile': '<!DOCTYPE html PUBLIC "-//WAPFORUM//DTD XHTML Mobile 1.2//EN" "http://www.openmobilealliance.org/tech/DTD/xhtml-mobile12.dtd">',
  'plist': '<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">'
}
```

### Accessability

I assumed the accessability test refers to the presence of the `alt` attribute on the anchors. If the anchor has an `alt` attribute and it is not empty, it passes the test. This test is far from perfect because CSS styling can easily convert an otherwise accessible anchor invisible or impossible to click on some devices.

### Presence of a Login Form

I assumed that a website with a login form will have a password field. This a
multilingual advantage over regexing the document body for a 'login' string 

### Links

I assumed 'links' refers exclusively to anchors, not to images.

I assumed an internal link is the one that hits exactly the same host
even with using different protocols (HTTP/HTTPS). That means that a different subdomains will be considered an external link.

## API response examples

```
> curl -t POST --data "url=http://google.com" localhost:8080/apiv1/urlAnalyzer
{ pageTitle: 'Google',
  htmlVersion: 'html',
  headerCount: { h1: 0, h2: 1, h3: 0, h4: 0, h5: 0, h6: 0 },
  anchors: 
   { totalAnchors: 32,
     hostAnalisys: { internalAnchors: 0, incompleteAnchors: 0, externalAnchors: 32 },
     accesibilityAnalysis: { accessibleAnchors: 1, innaccessibleAnchors: 31 } },
  hasLoginForm: false }
```

```
{ pageTitle: 'Slashdot: News for nerds, stuff that matters',
  htmlVersion: 'html',
  headerCount: { h1: 2, h2: 22, h3: 6, h4: 0, h5: 0, h6: 0 },
  anchors: 
   { totalAnchors: 292,
     hostAnalisys: 
      { internalAnchors: 1,
        incompleteAnchors: 29,
        externalAnchors: 262 },
     accesibilityAnalysis: { accessibleAnchors: 69, innaccessibleAnchors: 223 } },
  hasLoginForm: true }
```

```
curl -t POST -i --data "url=incorrect.URI" localhost:8080/apiv1/urlAnalyzer
HTTP/1.1 400 Bad Request
X-Powered-By: Express
Content-Type: application/json; charset=utf-8
Content-Length: 126
ETag: W/"7e-8e928eQCfsVd24CaJgCV0A"
Date: Tue, 16 Aug 2016 08:07:58 GMT
Connection: keep-alive

{"status":400,"message":"URL is not a proper formatted URL like protocol://user:pass@host.com:8080/p/a/t/h?query=string#hash"}%
```

## Tests

I used mocha+should for the unit tests.

Run the unit tests with 
`npm run test`

The output should be similar to this
```
  analyzeAnchors
    ✓ should have anchors
    ✓ should have 2 totalAnchors
    ✓ should have one internal anchor
    ✓ should have one external anchor
    ✓ should have accessibility Analysis
    ✓ should have one accesible anchor
    ✓ should have one innaccesible anchor

  analyzeHeaders
    ✓ should have headerCount
    ✓ should have one H1
    ✓ should have two H2
    ✓ should have zero H3

  analyzeHtmlVersion
    ✓ should have HTML htmlVersion

  analyzeLoginForm
    ✓ should have login Form


  13 passing (12ms)
```

# Frontent

Client side the app uses React for the logic, and Bootstrap for layout and styling. Native XMLHttpRequest() is used just for the AJAX request. It launches in development with nodemon for automatic restarts. The Frontend uses EC6 code transpiled with Babel. The app is packaged using webpack. As most of the logic is on the backend side, there are no functional tests.

Screencast:
![Screencast](https://bytebucket.org/dgrabla/inmoscout24challenge/raw/258dc145000ed5be906ce66d458bc93a0370d8c9/frontend_demo.gif)


# Further improvements - Where to go from here

- Remove all console.log commands and use a logging library like winston
- Frontend needs much improvement. From the current state, I would send it to the
  UI/UX/Design team for ideas, artwork and a mockup. Then I would implement the
  improved layout with CSS and animations with CSS/JS. For example there are no
  transitions, and the user needs some visual indication that something is
  going on when the button is clicked.
- Better test coverage. There are internal unit tests, but it doesn't cover the actual HTTP request