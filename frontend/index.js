// I prefer to use a logger like winston, here I will just use the console
// Remove of console for production
if (process.env.NODE_ENV !== 'production') {
  const console = {};
  console.log = function () {};
  console.error = function () {};
  console.info = function () {};
}


import React from 'react';
import ReactDOM from 'react-dom';
import WebAnalyzerUI from './components/webAnalyzerUI/webAnalyzerUI';

import htmlStyles from './index.scss'

ReactDOM.render(
  <WebAnalyzerUI/>,
  document.getElementById('main')
);
