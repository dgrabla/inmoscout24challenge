const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  devtool: 'eval',
  output: {
    filename: 'bundle.js',
  },
  plugins: [
    new CopyWebpackPlugin([
      {
        from: 'frontend/index.html'
      },
      {
        from: 'frontend/css/bootstrap.min.css'
      }
    ])
  ],
  module: {
    loaders: [{
      test: /\.js$/,
      loaders: ['babel'],
      exclude: /node_modules/,
    },
    { 
      test: /\.scss$/, 
      loaders: ["style", "css", "sass"],
    }
    ]
  }
};
