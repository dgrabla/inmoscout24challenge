import React from 'react';

export default class AnalysisTable extends React.Component {

  constructor(props) {
    super(props);
  }


  render() {
    return (
    <div>
    <h2>Test result for {this.props.data.url}</h2>
      <table className="table">
        <thead>
        <tr>
          <th>Test</th>
          <th>Result</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>Version of HTML on the site</td>
          <td>{this.props.data.htmlVersion}</td>
        </tr>
        <tr>
          <td>Title of the page</td>
          <td>{this.props.data.pageTitle}</td>
        </tr>
        <tr>
          <td>Has this page a login form?</td>
          <td>{this.props.data.hasLoginForm.toString()}</td> 
        </tr>
        <tr>
          <td>Number of headers</td>
          <td>
          H1: {this.props.data.headerCount.h1},
          H2: {this.props.data.headerCount.h2},
          H3: {this.props.data.headerCount.h3},
          H4: {this.props.data.headerCount.h4},
          H5: {this.props.data.headerCount.h5},
          H6: {this.props.data.headerCount.h6}</td>
        </tr>
        <tr>
          <td>Anchor Analysis</td>
          <td>
          Total number of anchors: {this.props.data.anchors.totalAnchors},
          Internal Anchors: {this.props.data.anchors.hostAnalysis.internalAnchors},
          External Anchors: {this.props.data.anchors.hostAnalysis.externalAnchors},
          Accessible Anchors: {this.props.data.anchors.accessibilityAnalysis.accessibleAnchors},
          External Anchors: {this.props.data.anchors.accessibilityAnalysis.innaccessibleAnchors},
          </td>
        </tr>
        </tbody>
      </table>
    </div>
    );
  }
}

AnalysisTable.propTypes = {
  data: React.PropTypes.object
};

