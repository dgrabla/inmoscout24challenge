// DGB 2016-08-02 19:25
// Entry point for the component

import React from 'react';
import _ from 'lodash';
import getApiEndpoint from '../../utils/api';

import ComponentStyle from './webAnalyzerUI.scss'
import AnalysisTable from './components/AnalysisTable';

export default class WebAnalyzerUI extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      currentUrl: false,
      error: false
    };
    // DGB 2016-08-02 19:26 EC6 style doesn't bind by default
    this.onClickedButton = this.onClickedButton.bind(this);
    this.onInputChange = this.onInputChange.bind(this);
  }
  componentDidMount() {
    console.log('Loaded WebAnalyzerUI - main component');
  }

  onClickedButton(event) {
    console.log('Clicked Button');
    event.preventDefault();
    this.setState({
      currentUrl: event.target.url.value,
    });
   
    var xhr;
    xhr = new XMLHttpRequest();
    xhr.open('POST', getApiEndpoint('urlAnalyzer'), true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var self = this;
    xhr.onload = function() {
    if (xhr.status === 200) {
      self.setState({ currentUrlAnalysis: JSON.parse(xhr.response), error: false });
    }
    else if (xhr.status !== 200) {
      self.setState({ error: JSON.parse(xhr.response).message })
    }
};
    xhr.send('url='+this.state.currentUrl);
  }

  onInputChange(event) {
    console.log('Changed Input');
    console.log(event)
    this.setState({
      currentUrl: event.target.value,
    });
  }

  render() {
    var toolbarUI, basicUI;

    toolbarUI = <form className="form-inline text-center" onSubmit={this.onClickedButton}>
      <div className="form-group form-group-lg">
      <div className="input-group">
      <div className="input-group-addon">URL</div>
      <input type="text" className="form-control" name="url" onChange={this.onInputChange} placeholder="http://www.google.com" value={(this.state.currentUrl!== false)?this.state.currentUrl:''}/> 
      </div>
      </div>
      <input className="btn btn-primary btn-lg" type="submit" value="Send"/>
      </form>;

    if (this.state.error){
      return (
        <div className="WebAnalyzerUI">
          {toolbarUI}
          <div className="error">
          <p className='bg-warning'>
            {this.state.error}
          </p>
          </div>
        </div>
      );
    } else if (this.state.currentUrlAnalysis) {
      return (
         <div className="WebAnalyzerUI">
          {toolbarUI}
          <AnalysisTable data={this.state.currentUrlAnalysis}/>
        </div> 
      );
    } else {
      return (
        <div className="WebAnalyzerUI">
          {toolbarUI}
        </div>
      )
    }
  }
}

WebAnalyzerUI.propTypes = {
  data: React.PropTypes.object,
};
