const apiHost = 'http://localhost:8080';

export default function (name, param) {
  const apiEndpoints = {
    urlAnalyzer: function (url) {
      return '/apiv1/urlanalyzer';
    },
  };
  return apiHost + apiEndpoints[name](param);
}

